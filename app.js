var Twit = require('twit');
var fs = require('fs');
var config = require('./auth-config/twitter-full-config');

var T = new Twit(config);
var now = (new Date()).getTime();
console.log('... Collecting tweets ...')
var stream = T.stream('statuses/sample');
stream.on('tweet', function(data) {

	fs.appendFile('dump/stream' + now + ".json", JSON.stringify(data),
		function(err) {
			if (err) {
				console.log(err);
			}
		});
	
});
